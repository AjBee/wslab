# CURL Cheat Sheet
https://github.com/curl/curl-cheat-sheet
https://terminalcheatsheet.com/guides/curl-rest-api
https://www.codepedia.org/ama/how-to-test-a-rest-api-from-command-line-with-curl/
https://www.baeldung.com/curl-rest


``` bash
curl -d "@cart.json" -H "Content-Type: application/json" -X POST http://localhost:5000/api/update_cart

curl -d '{"c1":"2","price1":"150","c2":"1","price2":"1000","c3":1,"price3":"750"}' -H "Content-Type: application/json" -X POST http://localhost:5000/api/update_cart
```

# Postman
https://www.postman.com
